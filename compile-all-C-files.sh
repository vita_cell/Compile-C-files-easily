#!/bin/bash
# C compiling script with GUI


# DEPENDENCIES CHECK
if [ $(which zenity 2>/dev/null) ]; then
  echo "zenity found"
else
  echo "zenity not found"
  echo "Please install zenity first"
  exit 1
fi

if [ $(which gcc 2>/dev/null) ]; then
  echo "GCC found"
else
  echo "GCC not found"
  zenity --error --width=200 --height=120 --title "Dependency error" --text "GCC not found"
  echo "Please install GCC first or check GCC path"
  exit 1
fi

pango=$(ls /usr/lib | grep pango)
glib=$(ls /usr/lib | grep -o glib)
atk=$(ls /usr/lib | grep -o atk)
gdk=$(ls /usr/lib | grep -o gdk)
pixbuf=$(ls /usr/lib | grep -o pixbuf)
cairo=$(ls /usr/lib | grep -o cairo)

if [ -z "$pango" ]; then
  echo "Pango not found"
  zenity --error --width=200 --height=120 --title "Dependency error" --text "Pango not found"
  echo "Please install Pango first"
  exit 1
fi

if [ -z "$glib" ]; then
  echo "Glib not found"
  zenity --error --width=200 --height=120 --title "Dependency error" --text "Glib not found"
  echo "Please install Glib first"
  exit 1
fi

if [ -z "$atk" ]; then
  echo "Atk not found"
  zenity --error --width=200 --height=120 --title "Dependency error" --text "Atk not found"
  echo "Please install Atk first"
  exit 1
fi

if [ -z "$gdk" ]; then
  echo "Gdk not found"
  zenity --error --width=200 --height=120 --title "Dependency error" --text "Gdk not found"
  echo "Please install Gdk first"
  exit 1
fi

if [ -z "$pixbuf" ]; then
  echo "Pixbuf not found"
  zenity --error --width=200 --height=120 --title "Dependency error" --text "Pixbuf not found"
  echo "Please install Pixbuf first"
  exit 1
fi

if [ -z "$cairo" ]; then
  echo "Cairo not found"
  zenity --error --width=200 --height=120 --title "Dependency error" --text "Cairo not found"
  echo "Please install Cairo first"
  exit 1
fi
# DEPENDENCIES CHECK








zenity --question --width=460 --height=120 --title "GNU/Linux found a critical error in your system" --ok-label="Definitely" \
--cancel-label="Just format DISK C:\\" --text "Microsoft Windows is installed on your system \n \n Do you want to uninstall Microsoft Windows? \n \n This will wipe your Microbugs Windoze partition" ; echo $

option="$(zenity --width=470 --height=240 --list --text "Compile C files with GNU GCC compiler" --radiolist --column "Operation" --column="Description" TRUE "Compile a C file" FALSE "Compile C file with GTK2 GUI" FALSE "Compile all C files inside the folder with GTK2 GUI")"

echo "Selected option is: " "$option"

#file="$(zenity –-width=360 –-height=320 –-list –-title “Lanzador” –-column “Compile a C file” “Compile multiple C files” “Compile all C files inside the folder”)"
#echo "Selected file is: " "$file"

if [ "$option" == "Compile a C file" ]; then
cfile=$(zenity --file-selection --title="Select a C source code file")
echo "Selected file is: " "$cfile"
#"$csavefile"="${cfile::-2}"
#echo "${cfile::-2}"
gcc -o "${cfile::-2}" "$cfile"
echo Compiled success!
zenity --info --width=230 --height=120 --title "Compilation success" --text "Your binary file is ready"
exit 0

elif [ "$option" == "Compile C file with GTK2 GUI" ]; then
cfile=$(zenity --file-selection --title="Select a C source code file")
echo "Selected file is: " "$cfile"
#"$csavefile"="${cfile::-2}"
#echo "${cfile::-2}"
gcc -o "${cfile::-2}" "$cfile"  `pkg-config --libs --cflags gtk+-2.0`
echo Compiled success!
zenity --info --width=230 --height=120 --title "Compilation success" --text "Your binary file is ready"
exit 0

elif [ "$option" == "Compile all C files inside the folder with GTK2 GUI" ]; then
cfolder=$(zenity --file-selection --title="Select a folder with C source code files" --directory)
echo "Selected directory is: " "$cfolder"
cd "$cfolder"
for i in *.c;  
do   
  gcc "$i" -o ${i%.c} $(pkg-config --libs --cflags gtk+-2.0); 
done

echo Compiled success!
zenity --info --width=230 --height=120 --title "Compilation success" --text "Your binary files are ready"
exit 0

else
exit 0

fi

# gcc -o centering-window-by-code centering-window-by-code.c  `pkg-config --libs --cflags gtk+-2.0`

echo Compiled success!